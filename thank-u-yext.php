<?php
/*
Plugin Name: Yext Best Thing
Version: 0.0.1
Description: Import Yext API data into a custom post type on the Winneshiek Medical Center website.
Author: Happy Medium
Author URI: https://itsahappymedium.com
*/

if (!class_exists('Yext_Import')) {
  define('YEXT_VERSION', '0.0.1');
  define('YEXT_URL', plugin_dir_url(__FILE__));
  define('YEXT_PATH', plugin_dir_path(__FILE__));
  define('YEXT_BASENAME', plugin_basename(__FILE__));
  define('YEXT_KEEP_LOGS', true);

  class Yext_Import {
    public $admin_capability;
    public $import_tasks = array();
    public $requests = array();

    function __construct() {
      require_once(YEXT_PATH . 'vendor/autoload.php');

      add_action('admin_bar_menu', array(&$this, 'admin_bar'), 99);
      add_action('admin_head', array(&$this, 'admin_js'));
      add_action('after_setup_theme', array(&$this, 'init_config'));
      add_action('yext_clean_logs', array(&$this, 'clean_logs'));
      add_action('wp_ajax_yext_clear_cache', array(&$this, 'clear_cache'));
      add_action('wp_ajax_yext_run_import_tasks', array(&$this, 'run_import_tasks'));

      if (defined('YEXT_KEEP_LOGS') && YEXT_KEEP_LOGS && !wp_next_scheduled('yext_clean_logs')) {
        wp_schedule_event(time(), 'daily', 'yext_clean_logs');
      }

      register_deactivation_hook(__FILE__, array(&$this, 'deactivation'));
    }

    function add_import_task($config) {
      if (!is_array($config)) throw new Exception('config should be an array');

      $default_config = array(
        'request'         => key(reset($this->requests)),
        'post_author'     => 1,
        'post_status'     => 'publish',
        'post_title'      => '',
        'post_type'       => 'post',
        'missing_status'  => false,
        'set_headshot'    => false
      );

      $task_id = count($this->import_tasks);

      add_action('yext_import_task_' . $task_id, array(&$this, 'import_posts'), 10, 1);
      if (isset($config['interval']) && !wp_next_scheduled('yext_import_task_' . $task_id, array($task_id))) {
        wp_schedule_event(time(), $config['interval'], 'yext_import_task_' . $task_id, array($task_id));
      }

      $this->import_tasks[] = array_merge($default_config, $config);
    }

    function add_request($config) { 
      if (!is_array($config)) throw new Exception('config should be an array');
      if (!isset($config['api_key'])) throw new Exception('API Key is required');
      if (!isset($config['id'])) throw new Exception('Request ID is required');

      $datetime = new DateTime();
      $date = $datetime->format('Ymd');
      $request_id =  isset($config['id']) ? $config['id'] : count($this->requests);
      $default_config = array(
        'date'  => $date,
        'count' => 50,
        'id'    => $request_id
      );

      $this->requests[$request_id] = array_merge($default_config, $config);
    }

    function admin_bar($wp_admin_bar) {
      if (!is_admin() || !current_user_can($this->admin_capability)) return;

      $wp_admin_bar->add_node(array(
        'id'      => 'yext',
        'title'   => 'Yext',
        'parent'  => false
      ));

      $wp_admin_bar->add_node(array(
        'id'      => 'yext-run_import_tasks',
        'title'   => 'Run Import',
        'parent'  => 'yext',
        'href'    => '#',
        'meta'    => array(
          'onclick' => 'yext_ajax("run_import_tasks");'
        )
      ));
    }

    function admin_js() {
      if (!current_user_can($this->admin_capability)) return;
      ?>
      <script type="text/javascript">
      function yext_ajax(action) {
        jQuery('body').css('cursor', 'wait')

        jQuery.ajax({
          type: 'post',
          url: '<?=admin_url('admin-ajax.php')?>',
          data: {
            action: 'yext_' + action
          }
        })
          .done(function (response) {
            if (response.success) {
              var count = response.data.count

              if (action === 'clear_cache') {
                alert('Successfully cleared ' + count + ' rows of cache.')
              }

              if (action === 'run_import_tasks') {
                alert('Successfully imported/updated ' + count + ' posts.')
              }
            } else {
              alert('An unknown error occured.')
            }
          })
          .fail(function () {
            alert('An unknown error occured.')
          })
          .always(function(response) {
            jQuery('body').css('cursor', '')
            console.log(response)
          })
      }
      </script>
      <?php
    }

    function api_request($request = array()) {
      $yext_api = $request['api_key'];
      $account_id = $request['account_id'];
      $query = $request['query'];
      $date = $request['date'];
      $count = $request['count'];
      $api_info = "?api_key=".$yext_api."&v=".$date."&limit=".$count;
      $url = "https://api.yext.com/v2/accounts/".$account_id."/".$query.$api_info;
      $response = wp_remote_request($url);
      return(array) json_decode(wp_remote_retrieve_body($response), true);
    }

    function bool_it($value = "") {
      if($value == 1 || $value == '1' || $value == 'yes' || $value == 'true') {
        return true;
      }
      return false;
    }

    function clean_logs($keep_recent = 5) {
      if (defined('YEXT_LOGS_PATH')) {
        $log_dir = YEXT_LOGS_PATH;
      } else {
        $upload_dir = wp_upload_dir();
        $log_dir = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'yext';
      }

      if (!file_exists($log_dir)) return;

      if (defined('YEXT_KEEP_LOGS') && is_numeric(YEXT_KEEP_LOGS)) {
        $keep_recent = YEXT_KEEP_LOGS;
      }

      $logs = scandir($log_dir, SCANDIR_SORT_DESCENDING);
      $logs = array_diff($logs, array('..', '.'));

      if (is_array($logs)) {
        $logs = array_slice($logs, $keep_recent);

        foreach ($logs as $log) {
          unlink($log_dir . DIRECTORY_SEPARATOR . $log);
        }

        if ($keep_recent === 0) rmdir($log_dir);

        return count($logs);
      }

      return $logs;
    }

    function deactivation() {
      $this->clear_cache();
      $this->clean_logs(0);
    }

    function filter_results_function($results = array(), $filter = "") {
      $final_results = array();
      foreach($results['response']['entities'] as $provider) {
        if($provider['meta']['entityType'] == $filter) {
          array_push($final_results, $provider);
        }
      }
      return $final_results;
    }

    function format_phone($phone = "") {
      $strip = preg_replace("/[^0-9]/", "", $phone);
      if(strlen($strip) > 9) {
        $substr = substr($strip, -10);
        $area_code = substr($substr, 0, 3);
        $phone_3 = substr($substr, 3, 3);
        $phone_4 = substr($substr, 6);
        $phone = $area_code .'.'. $phone_3 .'.'. $phone_4;
      }
      return $phone;
    }

    function format_yext_education($education = array()) {
      $edu = array();
      if(isset($education['educationList'])) {
        foreach($education['educationList'] as $edu_record) {
          $data = array('degree_school'  => $edu_record['institutionName']);
          array_push($edu, $data);
        }
      }
      return $edu;
    }

    function format_yext_boards($boards = array()) {
      $board = array();
      if(isset($boards['certifications'])) {
        foreach($boards['certifications'] as $cert) {
          $data = array('credential'  => $cert);
          array_push($board, $data);
        }
      }
      return $board;
    }

    function format_yext_keywords($provider = array()) {
      $keyword_array = array();
      if(isset($provider['keywords'])) {
        $keyword_array += $provider['keywords'];
      }
      if(isset($provider['conditionsTreated'])) {
        $keyword_array += $provider['conditionsTreated'];
      }
      if(isset($provider['services'])) {
        $keyword_array += $provider['services'];
      }
      if(!empty($keyword_array)) {
        $keyword_array = implode(", ", $keyword_array);
      }
      return $keyword_array;
    }

    function get_request($request_id = null) {
      if (isset($request_id)) {
        if (array_key_exists($request_id, $this->requests)) {
          $request = $this->requests[$request_id];

        }
      } else {
        $request = reset($this->requests);
      }

      if (!isset($request)) throw new Exception('Invalid request ID');

      return $request;
    }

    function import_posts($task_id) {
      set_time_limit(0);
      ini_set('memory_limit', '-1');

      do_action('yext_before_import_task', $task_id);

      $this->log("Import task $task_id started");

      $task = $this->import_tasks[$task_id];
      $request = $this->get_request(isset($task['request']) ? $task['request'] : null);
      $imported_ids = array();
      $unique_keys = array();

      try {
        $results = $this->api_request($request);
      } catch (Exception $e) {
        $this->log('Query Error: ' . $e->getMessage());
        return $e;
      }

      if(isset($request['filter'])) {
        add_filter('filter_results', array(&$this, 'filter_results_function'), 10, 3);
        $results = apply_filters('filter_results', $results, $request['filter']);
      }

      foreach($results as $key => $provider) {

        if(!($this->yext_provider_exists($provider))) {

          $post = array(
            'post_title'  => $provider['name'],
            'post_type'   => $task['post_type'],
            'post_author' => $task['post_author'],
            'post_status' => $task['post_status']
          );

          $post_id = wp_insert_post($post);
          array_push($imported_ids, $post_id);

          if($task['set_headshot']) {
            $this->yext_set_headshot($provider, $post_id);
          }

          $post_meta = array(
            'npi_number'                =>  $provider['npi'],
            'yext_id'                   =>  $provider['meta']['id'],
            'yext_import'               =>  true,
            'accepting_new_patients'    =>  $this->bool_it($provider['acceptingNewPatients']),
            'appointment_phone_number'  =>  $this->format_phone($provider['mainPhone']),
            'gender'                    =>  strtolower($provider['gender']),
            'education'                 =>  $this->format_yext_education($provider),
            'boards_and_certifications' =>  $this->format_yext_boards($provider),
            'special_interests'         =>  $provider['description'],
            'tags'                      =>  $this->format_yext_keywords($provider)
          );

          foreach($post_meta as $key => $data) {
            update_field($key, $data, $post_id);
          }
          do_action('yext_after_post_import', $post, $post_meta, $post_id);
          $this->log("Import Successful: ".$provider['name']);
        }
      }

      do_action('yext_after_import_task', $task_id, $imported_ids);

      $count = count($imported_ids);

      $this->log("Import task $task_id finished: imported $count posts");
      write_log("Import task $task_id finished: imported $count posts");
      return $imported_ids;
    }

    function init_config() {
      do_action('yext_config');
      $this->admin_capability = apply_filters('yext_capability', 'manage_options');
    }

    function log($msg) {
      if (!defined('YEXT_KEEP_LOGS') || !YEXT_KEEP_LOGS) return;

      if (defined('YEXT_LOGS_PATH')) {
        $log_dir = YEXT_LOGS_PATH;
      } else {
        $upload_dir = wp_upload_dir();
        $log_dir = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . 'yext';
      }

      $log_file = $log_dir . DIRECTORY_SEPARATOR . date('Ymd') . '.log';

      if (!file_exists($log_dir)) mkdir($log_dir);

      $entry = '[' . date('m/d/Y h:i:s A') . '] ' . $msg;
      return file_put_contents($log_file, $entry . PHP_EOL, FILE_APPEND);
    }

    function run_import_tasks() {
      if (!current_user_can($this->admin_capability)) return;

      $count = 0;

      foreach($this->import_tasks as $task_id => $task) {
        $count += count($this->import_posts($task_id));
      }

      if (defined('DOING_AJAX') && DOING_AJAX) {
        wp_send_json_success(array('count' => $count));
      }

      return $count;
    }

    function yext_provider_exists($provider = array()) {
      $args = array(
        'post_type'				=> 'provider',
        'post_status'			=> array('publish', 'pending'),
        'numberposts'     => -1,
        'meta_query'      => array(
          'relation'      => 'OR',
          array(
            'key'         => 'yext_id',
            'value'       => $provider['meta']['id']
          ),
          array(
            'key'         => 'npi_number',
            'value'       => $provider['npi']
          )
        )
      );
      $existing_post = get_posts($args);

      if(!empty($existing_post)) {
        write_log('#### '.$provider['name'].' exists ####');
        return true;
      }
      write_log('#### '.$provider['name'].' does not exist ####');
      return false;
    }

    function yext_set_headshot($provider = array(), $post_id = 0) {
      require_once ABSPATH . 'wp-admin/includes/file.php';
      require_once ABSPATH . 'wp-admin/includes/media.php';
      require_once ABSPATH . 'wp-admin/includes/image.php';

      if(isset($provider['photoGallery']) && !empty($provider['photoGallery'])) {
        if(isset($provider['photoGallery'][0]['image']['url'])) {
          $image_url = $provider['photoGallery'][0]['image']['url'];
          $url = esc_url_raw($image_url);
          $timeout = 5;
          $temp_file = download_url($url, $timeout);
          if (!is_wp_error( $temp_file)) {
            $filename = preg_replace("/[^A-Za-z0-9 ]/", "", $provider['name']);
            $wp_file_type = wp_check_filetype(basename($url));
            $filemime = $wp_file_type['type'];
            $file = array(
              'name'     => $filename.'_'.basename($url),
              'type'     => $filemime,
              'tmp_name' => $temp_file,
              'error'    => 0,
              'size'     => filesize($temp_file),
            );
            $image_id = media_handle_sideload( $file, $post_id, NULL);
            return set_post_thumbnail($post_id, $image_id);
          }
        }
      }
    }
  }

  function yext_import() {
    global $yext_import;
    if (!isset($yext_import)) $yext_import = new Yext_Import();
    return $yext_import;
  }

  yext_import();
}
?>
