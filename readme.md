# Yext Best Thing WordPress Plugin


## Requirements

* PHP v7.4 or greater
* WordPress v5.8 or greater

## Installation

Add the following to your `composer.json` file:

```json
"repositories": [
  {
    "type": "git",
    "url": "git@bitbucket.org:itsahappymedium/yext-best-thing.git"
  }
],
"require": {
  "itsahappymedium/yext-best-thing": "[INSERT VERSION HERE]"
}
```

## Configuration
```php
<?php
add_action('yext_config', 'yext_config');
  function yext_config() {
    yext_import()->add_request(array(
      'id'          => 'providerImport',
      'api_key'     => YEXT_API, // define in config.yml
      'account_id'  => 'me',
      'query'       => 'entities',
      'filter'      => 'healthcareProfessional'
    ));
    yext_import()->add_import_task(array(
      'request'           => 'providerImport',
      'post_type'         => 'provider',
      'post_author'       => 1,
      'post_title'        => '%name%',
      'post_status'       => 'pending',
      'interval'          => 'daily',
      'missing_status'    => 'trash',
      'set_headshot'      => true
    ));
  }
?>
```
