<?php return array(
    'root' => array(
        'pretty_version' => '0.0.1',
        'version' => '0.0.1.0',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'itsahappymedium/yext-best-thing',
        'dev' => true,
    ),
    'versions' => array(
        'itsahappymedium/yext-best-thing' => array(
            'pretty_version' => '0.0.1',
            'version' => '0.0.1.0',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
